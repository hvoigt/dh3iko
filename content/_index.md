## Welcome fellow ham

- [My QRZ page](https://www.qrz.com/db/dh3iko)
- [Private page](https://www.hvoigt.net/ham/)
- [Impressum / Contact](https://www.hvoigt.net/kontakt/)

Heiko (DH3IKO)

## Papagei: Natural CW Keyer

{{< yt "https://www.youtube.com/embed/ihrdmn_cpCA" >}}

## Hamradio Club

My ham radio club DARC E13 Beispieländerung:

- [Amateurfunk im Alstertal](https://amateurfunk-im-alstertal.de/)
- [DARC](https://www.darc.de/home/)
