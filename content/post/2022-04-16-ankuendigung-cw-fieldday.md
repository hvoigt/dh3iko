---
title: Ankündigung E13 CW Fieldday 2022
date: 2022-04-16
bigimg: [{src: "/img/dr1e-p_map_fixed.jpg", desc: "QSO map"}]
---

Leider mussten wir aufgrund von Corona die CW Fielddays für 2020 und
2021 ausfallen lassen.  Dieses Jahr sieht die Lage sehr vielversprechend
aus sodass wir davon ausgehen wieder vom Feld aktiv werden zu können.

<!--more-->

Da es die letzten Male([2018][d18], [2019][d19]) so viel Spaß gemacht
hat, hoffen wir wie jedes Jahr auf noch mehr Aktivität. DH3IKO hat sich
bereit erklärt wieder als Ansprechpartner die Organisation zu
übernehmen. Der Fieldday lebt natürlich von der Teilnahme und je mehr
Teilnehmer sich finden umso mehr können wir ausprobieren und machen.

## Zeitraum

Mittlerweile hat es sich eingebürgert das wir schon am Donnerstag
anreisen, um die Ausnutzung des geselligen Beisammenseins zu maximieren.
D.h. wir werden *vom 2.-5. Juni 2022* auf dem Fielddayplatz campieren.

Die Vorbereitungen für den diesjährigen CW Fieldday sind also im Gange.

### Termine

- Am 10.5. Treffen aller Interessierten zur Vorbesprechung per
  [meeting.dl0at.de](https://meeting.dl0at.de/) (Falls Passwort nicht
  bekannt vorher bei DH3IKO nachfragen)
- Am Wochenende 21.5. potentieller Probeaufbau auf dem Fielddayplatz
- 2.6. Anreise auf dem Fielddayplatz

### Kontakt

Bei Interesse meldet euch bei Heiko (DH3IKO). Er hat eine Emailaddresse mit
seinem Rufzeichen beim DARC.de. Alternativ auch gerne per Matrix-Chat.

Zur Koordinierung wollen wir den neuen Matrix Chat des DARC
ausprobieren. Einfachste Vorgehensweise:

1. Die [Elements App][ela] installieren
1. App öffnen und beim Verbinden Homeserver `chat.darc.de` auswählen.
1. Mit DARC Zugangsdaten verbinden.
1. Link der Gruppe [*E13 Hamburg-Alstertal*][ctr] öffnen und Desktop
   bzw. Mobil App auswählen.

Dieser Chat lässt sich bequem am Mobiltelefon sowie zuhause am Rechner
nutzen.

*Hinweis:* Falls ihr das erste Mal Matrix benutzt haltet euch an die
Schritte oben mit dem Elements Client auf dem Desktop oder Mobil das ist
am einfachsten. Wir haben festgestellt das es mobil im Browser Probleme
geben kann.

## Team

Wir sind eine entspanntes gemischte Team, die sich unter anderem übt
mit deutlich langsameren Signalen ein paar Punkte zu machen. Für uns
steht zuerst mal der Spaß an der Sache im Vordergrund.

Aber auch für die schnelleren Signale gibt es Interesse. Wir sind schon
deutlich schneller geworden als in den Anfängen.

Dazu kommt das Erfolgsgefühl, dass wenn man nicht zu zaghaft ist, sich
häufig auch die schnellste Station mal zum QRS Betrieb überreden lässt.
Das konnten wir in den letzten Jahren zeigen und es ist immer wieder
schön wenn man merkt das auf der anderen Seite die Handtaste rausgekramt
wird, um uns zu arbeiten.

Unser Vorteil ist, dass wir durch den portabel Betrieb viele Punkte für
andere bringen und damit recht gute Chancen haben auch mal
durchzukommen.

Wir hoffen diesen Jahr wieder mehr Teilnehmer zu finden als letztes
Jahr. Je mehr Teilnehmer desto mehr Antenne.

## Antennen

Folgende Optionen sind möglich:

### Inverted-L oder Inverted-V mit Hühnerleiter

Die einfachste Variante: Eine inverted-V mit ca. 60m
Gesamtlänge in 13 Metern oder mit zwei Abspannpunkten ein inverted-L.

Gespeist werden diese per Hühnerleiter vom Tuner CG-3000 und gut. Falls
wir ein kleines Team werden hat uns dieses Setup die letzten Jahre gute
Dienste geleistet.

### Spiderbeam mit Rotor

Ab 10 Personen lässt sich das [Paket vom WRTC][wrt] aufbauen.

### 4-Square Phased Vertical Antenna

Bei genug Interesse können wir [diesen Traum][4sq] aufbauen:

4 Vertikale Strahler zusammengeschaltet mit einer Phasenverschiebung
ergeben eine Richtwirkung. Mit 4 x 20m Masten lässt sich so z.b. eine
80m Richtantenne aufbauen.

Axel (DB1WA) hat einen Controller, brauchen wir "nur noch die Masten und
etwas Kabel" :)

## Klasse

Um flexibel im Antennenaufbau zu sein werden wir in der Klasse

- Portable, multi operator, low power, assisted

starten.

## Training

Wir können nur empfehlen die Abläufe beim Contest ein wenig einzuüben.
Auch wenn man nur F-Tasten drücken will ist es hilfreich den Ablauf im
Schlaf zu können.

Um die Contestoperation zu üben ist [Morse Runner][msr] von Alex
(VE3NEA) die ideale Software. Auch wenn die Bedienoberfläche in die
Jahre gekommen scheint, es simuliert die Funksignale so gut man fühlt
sich fast wie im echten Contest.

Wir werden die F-Tasten Belegung wie bei Morse Runner konfigurieren. Zum
Fieldday wird [N1MM][n1m] als Logging Software zum Einsatz kommen.

## Übernachtung

Für diejenigen die zwar Interesse haben, aber die Aussicht im Zelt zu
übernachten nicht Romantik, sondern Rückenschmerzen verursacht, finden
wir sicherlich auch eine Lösung. Meldet euch einfach.

Wir haben typischerweise ein paar Wohnwagen vor Ort sodass auf ein gutes
Bett nicht verzichtet werden muss.

tu 73 es hpe cuagn fd test de dh3iko

[ela]: https://element.io/get-started#download
[4sq]: https://k7nv.com/id16.htm
[msr]: http://www.dxatlas.com/morserunner/
[n1m]: https://n1mmwp.hamdocs.com/
[wrt]: http://www.wrtc2018.de/images/gallery/wrtcweek/contest/20180713_162647_e7dx_fotodedm9ee_cd1_7976.jpg
[d18]: https://www.amateurfunk-im-alstertal.de/ortsverband-e13/was-passiert-bei-uns/funkaktivit%C3%A4ten/1490-cw-fieldday-2018-ein-voller-erfolg
[d19]: https://www.amateurfunk-im-alstertal.de/ortsverband-e13/was-passiert-bei-uns/funkaktivit%C3%A4ten/1679-cw-fieldday-2019-vergn%C3%BCgen-auf-allen-ebenen
[mtx]: https://chat.darc.de/
[ctr]: https://matrix.to/#/!TpcamSYuvpnIVULuex:darc.de?via=darc.de

## Referenzen

- [Ausschreibung](https://www.darc.de/der-club/referate/conteste/iaru-region-1-fieldday/regeln/)
- Ankündigungen:
  - CW Fieldday:
    - [2018](https://www.amateurfunk-im-alstertal.de/ortsverband-e13/was-passiert-bei-uns/funkaktivitäten/1445-cw-fieldday-2018-in-planung)
    - [2019](https://amateurfunk-im-alstertal.de/funkbetrieb/funk-aktivitäten/1621-planung-für-den-cw-fieldday-im-juni-läuft-an)
    - [Absage 2020](https://www.amateurfunk-im-alstertal.de/funkbetrieb/2012-04-02-06-33-43/clubmeisterschaft-infos/1764-cw-fieldday-2020-abgesagt)
  - SSB Fieldday:
    - [2019](https://www.amateurfunk-im-alstertal.de/funkbetrieb/2012-04-02-06-33-43/ergebnisse/2019-contestergebnisse/1710-ssb-fieldday-2019)
    - [2020](https://www.amateurfunk-im-alstertal.de/funkbetrieb/2012-04-02-06-33-43/conteste-für-die-cm/1808-ssb-fieldday-e13-am-5-und-6-september-in-gross-boden)
- Ergebnisse:
  - CW Fieldday:
    - [2019](https://www.amateurfunk-im-alstertal.de/ortsverband-e13/was-passiert-bei-uns/funkaktivitäten/1679-cw-fieldday-2019-vergnügen-auf-allen-ebenen)
    - [2018](https://www.amateurfunk-im-alstertal.de/ortsverband-e13/was-passiert-bei-uns/funkaktivitäten/1490-cw-fieldday-2018-ein-voller-erfolg)
  - SSB Fieldday:
    - [2019](https://www.amateurfunk-im-alstertal.de/funkbetrieb/2012-04-02-06-33-43/ergebnisse/2019-contestergebnisse/1716-ssb-fieldday-2019-e13-war-dabei)
    - [2020](https://www.amateurfunk-im-alstertal.de/funkbetrieb/2012-04-02-06-33-43/ergebnisse/2020-contestergebnisse/1817-e13-ergebnis-ssb-fieldday)
