---
title: Ankündigung E13 CW Fieldday 2024
date: 2024-03-20
bigimg: [{src: "/img/dr1e-p_map_fixed.jpg", desc: "QSO map"}]
---

<!--![Karte Funkkontakte](/img/2019-CWFD-02.jpg)-->

Es ist soweit ein neues Jahr hat begonnen und so langsam erwacht alles aus dem Winterschlaf. Damit beginnt auch die Ankündigung zum E13 Fieldday 2024. Heiko (DH3IKO) hat sich bereit erklärt wieder als Ansprechpartner die Organisation zu übernehmen. Hier gehts [zur offiziellen Ausschreibung](https://www.darc.de/der-club/referate/conteste/iaru-region-1-fieldday/regeln/).

<!--more-->

![Fielddayplatz Sonnenuntergang](/img/funk-sunset.jpg)

Ein Fieldday lebt hauptsächlich von den Teilnehmenden und es ist mehr als nur Wettbewerb. Jeder der schon ein CW QSO geführt hat ist eingeladen teilzunehmen. Die Geschwindkeit spielt weniger eine Rolle. Wichtiger ist die Lust am Funken und lernen. Das heißt natürlich nicht das wir nicht ergeizig sind. Das schöne am CW Contest ist das es sich sehr gut mit dem Programm [Morserunner](https://www.dxatlas.com/MorseRunner/) zuhause am PC trocken üben lässt.

Da sich aufgrund der späten Ankündigung letztes Jahr nicht genug Teilnehmer gefunden haben, um den Fieldday stattfinden zu lassen, wollen wir dieses Jahr frühzeitig eine Liste der potentiellen Teilnehmer zusammenstellen.

Also wer hat Lust und Zeit? Bei Interesse meldet Euch bitte bei Heiko (DH3IKO) per Email: `Rufzeichen @ darc.de`

Sollten sich bis Mitte April genug Teilnehmer gefunden haben werden wir mit den Planungen beginnen. Als kleinen Vorgeschmack hier ein Video unseres SSB Fielddays im letzten September:

[![Video SSB Fieldday 2023](/img/video-img.jpg)](https://www.youtube.com/watch?v=blOCspkZP8Y)
